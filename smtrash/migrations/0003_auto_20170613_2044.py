# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-06-13 17:44
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('smtrash', '0002_auto_20170613_2044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 13, 17, 44, 57, 206000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='trash',
            name='created_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 13, 17, 44, 57, 199000, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='trash',
            name='time_before_cleaning',
            field=models.TimeField(default=1497375897.199),
        ),
    ]
