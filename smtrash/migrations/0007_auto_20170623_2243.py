# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-23 19:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('smtrash', '0006_auto_20170622_1639'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='is_completed',
        ),
        migrations.AddField(
            model_name='trash',
            name='path',
            field=models.CharField(default='C:\\Users\\Z50-70/.trash', max_length=50),
        ),
    ]
