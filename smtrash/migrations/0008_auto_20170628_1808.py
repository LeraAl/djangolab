# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-28 15:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('smtrash', '0007_auto_20170623_2243'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='trash',
        ),
        migrations.RenameField(
            model_name='file',
            old_name='path',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='file',
            name='task',
        ),
        migrations.AddField(
            model_name='file',
            name='abspath',
            field=models.CharField(default='abspath', max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='file',
            name='trash',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='smtrash.Trash'),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Task',
        ),
    ]
