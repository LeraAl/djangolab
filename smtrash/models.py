from __future__ import unicode_literals

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone
from datetime import timedelta
import os

# Create your models here.


class Trash(models.Model):
    name = models.CharField(max_length=50, default='Trash')
    path = models.CharField(max_length=50, default=os.path.expanduser('~/.trash'))
    created_time = models.DateTimeField(default=timezone.now)
    replace_when_remove = models.BooleanField(default=True)
    replace_when_restore = models.BooleanField(default=True)
    time_before_cleaning = models.DurationField(default=timedelta(1))
    size_to_clean = models.IntegerField(default=1024, validators=[MaxValueValidator(10000000000),
            MinValueValidator(0)])

    def __str__(self):
        return self.name


class File(models.Model):

    name = models.CharField(max_length=100)
    abspath = models.CharField(max_length=250)
    trash = models.ForeignKey(Trash, on_delete=models.CASCADE)

    def __str__(self):
        return self.abspath
