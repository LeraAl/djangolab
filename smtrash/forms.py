from django import forms
from smtrash.models import Trash, File
import os


class TrashForm(forms.ModelForm):

    class Meta:
        model = Trash
        fields = ['name', 'path', 'replace_when_restore', 'time_before_cleaning', 'size_to_clean']


class FileForm(forms.Form):

    trash = forms.ModelChoiceField(queryset=Trash.objects.all())