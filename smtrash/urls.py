from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'trash/create/$', views.create_trash, name='create_trash'),
    url(r'trash/(?P<trash_id>[0-9]+)/$', views.trash, name='trash'),
    url(r'task/create/(?P<trash_id>[0-9]+)/$', views.CreateTaskView.as_view(), name='create_task_for_trash'),
    url(r'task/create/$', views.CreateTaskView.as_view(), name='create_task'),
    url(r'task/get/tree/$', views.TreeView.as_view(), name='create_task_tree'),
    url(r'files/$', views.files, name='files'),
    url(r'restore/(?P<file_id>[0-9]+)/$', views.restore, name='restore'),
]
