from django.shortcuts import render, redirect
from smtrash.models import Trash, File
from smtrash.forms import TrashForm, FileForm
from smartrm import trash as tr
from django.forms import formset_factory
from django.http import JsonResponse
import json, os, logging, re
from django.views.generic import TemplateView


# Create your views here.


def index(request):
    trashes = Trash.objects.all()
    return render(request, 'smtrash/index.html', {'trashes': trashes})


def trash(request, trash_id):
    current_trash = Trash.objects.get(pk=trash_id)
    file_set = current_trash.file_set.all()

    return render(request, 'smtrash/trash.html', {'files': file_set, 'trash': current_trash, 'header': current_trash.name})


def files(request):
    files = File.objects.all()
    return render(request, 'smtrash/files.html', {'files': files, 'header': "All files" })


def create_trash(request):
    if request.method == "POST":
        form = TrashForm(request.POST)
        if form.is_valid():
            trash = form.save(commit=False)
            trash.replace_when_restore = True
            trash.replace_when_remove = True
            form.save()
            return render(request, 'smtrash/index.html', {'trashes': Trash.objects.all()})
    else:
        form = TrashForm()
        return render(request, 'smtrash/create_trash.html', {'form': form})


def restore(request, file_id):
    file_item = File.objects.get(pk=file_id)
    trash_item = file_item.trash
    remover = tr.Trash(trash_path=trash_item.path,
                       replace_when_remove=trash_item.replace_when_remove,
                       replace_when_restore=trash_item.replace_when_restore,
                       dry_run_rm_decorator=None,
                       dry_run_restore_decorator=None,
                       regex=False,
                       confirm_remove=lambda x: True,
                       confirm_go_into=lambda x: True,
                       error_handler=lambda x: None,
                       time_before_cleaning=trash_item.time_before_cleaning,
                       size_to_clean=trash_item.size_to_clean,
                       output_function=lambda x: None,
                       logger=None)
    remover.restore([file_item.name])
    file_item.delete()
    return redirect('/trash/' + str(trash_item.id))


class CreateTaskView(TemplateView):
    def get(self, request, *args, **kwargs):
        trash_id = kwargs.get('trash_id')
        if trash_id:
            form = FileForm(initial={'trash': trash_id})
        else:
            form = FileForm()
        return render(request, 'smtrash/create_task.html', {'form': form})


    def post(self, request, trash_id):
        FORMAT = "%(asctime)s - %(levelname)-8s: %(filename)-10s: %(funcName)s:  %(message)s "
        log_file = './smtrash.log'
        logging.basicConfig(format=FORMAT, filename=log_file, level=logging.DEBUG)
        logger = logging.getLogger('smtrash_logger')

        trash_item = Trash.objects.get(pk=trash_id)
        files_unicode = json.loads(request.POST.get('files'))
        filenames = map(lambda x: str(x), files_unicode)
        
        if str(request.POST.get('has_regex')) == 'on':
            temp_list = set()
            for filename in filenames:
                temp_list.add(filename)
                for path, dirs, files in os.walk(filename):
                    for dirname in dirs:
                        temp_list.add(os.path.join(path, dirname))
                    for fname in files:
                        temp_list.add(os.path.join(path, fname))
            filenames = temp_list
            logger.debug('all files {}'.format(filenames))
            regex = str(request.POST.get('regex'))
            filenames = filter(lambda x: re.match(regex, os.path.basename(x)), filenames)
            logger.debug('after regex {}'.format(filenames))
            
        filenames = self._handle_dirs(filenames)
        
        remover = tr.Trash(trash_path=trash_item.path,
                           replace_when_remove=trash_item.replace_when_remove,
                           replace_when_restore=trash_item.replace_when_restore,
                           dry_run_rm_decorator=None,
                           dry_run_restore_decorator=None,
                           regex=False,
                           confirm_remove=lambda x: True,
                           confirm_go_into=lambda x: True,
                           error_handler=lambda x: None,
                           time_before_cleaning=trash_item.time_before_cleaning,
                           size_to_clean=trash_item.size_to_clean,
                           output_function=lambda x: None,
                           logger=logger)
        removed_files = remover.remove(filenames)
        logger.debug('removed {}'.format(removed_files))
        for i in removed_files:
            File.objects.create(abspath=i, name=os.path.basename(i), trash=trash_item)
        return redirect('/trash/' + str(trash_id))

    @staticmethod
    def _handle_dirs(filenames):
        for i in filenames[:]:
            if os.path.isdir(i):
                filenames = filter(lambda x: not(i in x and i != x), filenames)
        return filenames


class TreeView(TemplateView):
    def get(self, request, *args, **kwargs):
        path = request.GET.get('id')
        if path == '#':
            #path = os.path.expanduser('~/')
            path = os.path.expanduser('~/')
            return JsonResponse(dict(id=path, text=os.path.basename(path), state=dict(opened=True, undetermined=True),
                            children=self._get_children(path)))
        else:
            return JsonResponse(self._get_children(path), safe=False)

    @staticmethod
    def _get_children(path):
        if os.path.isfile(path):
            return False
        children = os.listdir(path)
        to_return = list()
        for i in range(len(children)):
            child = os.path.join(path, children[i])
            dict_ch = dict()
            dict_ch['id'] = child
            dict_ch['text'] = os.path.basename(child)
            dict_ch['children'] = False if os.path.isfile(child) else True
            dict_ch['type'] = 'file' if os.path.isfile(child) else 'directory'
            dict_ch['state'] = "closed"
            to_return.append(dict_ch)
        return to_return